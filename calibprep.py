import os
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
import glob
import numpy as np
from astropysics.obstools import get_SFD_dust
from astropy.coordinates import SkyCoord, FK5, Galactic
from astropy.io import fits
import astropy.units as u
from time import strftime
from idlutils.pyspherematch import spherematch
#custom ports of useful idl functions that I wrote
import idlutils.idlutils as idl


#########CODE TO SELECT WELL-IMAGED STARS TO USE IN THE CALIBRATION CODE#######

#set the time the program began, to later see how long it toko
startingtime = strftime("%Y-%m-%d %H:%M:%S")

#read in sdss structure with measured positions, magnitudes, etc.
sdsslist = fits.open('sdssherc.fit')
sdss = sdsslist[1].data

#set up for plotting
outputplots = PdfPages('calibQApython.pdf')

#define matchlength in arcseconds
matchlength = 0.3

#cd to where the allstar photometry files are
os.chdir('mstr/')

masterfiles = glob.glob('*')
number1 = len(masterfiles)

for j in masterfiles:
	print j
	id, x, y, mag, mag_error, sky, thresh, chi, sharp, ra, dec, id2, chip, extinction = np.loadtxt(j, unpack=True, skiprows=3)
	#function I wrote to pick a specific line out of a file
	line2data = idl.read_line(j, 2)
	#function I wrote to count how many lines in a file
	nstars = idl.file_lines(j)-3
	airmass = float(line2data[10])
	filt = line2data[11]
	mjd = float(line2data[12])

################################################################################

	#match coordinates between sdss and our data
	m1, m2, d12 = spherematch(ra, dec, sdss.ra, sdss.dec, tol=matchlength/3600., nnearest=1)
	print 'Number of matches: '+str(len(m1))

	#correct the magnitudes for extinction
	#convert to galactic coordinates
	ell, bee = idl.glactc(ra, dec)
	#determine e(b-v) values with astropysics
	ebv = get_SFD_dust(ell, bee, dustmap='/home/cgarling/idl/dust/CodeIDL/SFD_dust_4096_%s.fits', interpolate=True)
	#use the schlafly finkbeiner correction coefficients
	A_g = 3.303*ebv
	A_r = 2.285*ebv
	A_i = 1.698*ebv
	if filt == 'g': mag = mag - A_g
	if filt == 'r': mag = mag - A_r
	if filt == 'i': mag = mag - A_i
################################################################################################
	#write data into a structure for organization
	stars = np.recarray(len(m1), dtype=[('x', float), ('y',float), ('mag', float), ('mag_error', float), ('chi',float),('sharp',float),('ra',float),('dec',float),('chip',float),('airmass',float),('filter','|S1'),('mjd',float),('filename','|S20'),('sdssr',float),('sdssrerr',float),('sdssg',float),('sdssgerr',float),('sdssi',float),('sdssierr',float)  ])

	stars.x=x[m1]
	stars.y=y[m1]
	stars.mag=mag[m1]
	stars.mag_error=mag_error[m1]
	stars.chi=chi[m1]
	stars.sharp=sharp[m1]
	stars.ra=ra[m1]
	stars.dec=dec[m1]
	stars.chip=chip[m1]
	stars.airmass=airmass
	stars.filter = filt
	stars.mjd=mjd
	stars.filename=j[:-9]

	#write SDSS data for matches into structure 
	stars['sdssr'] = sdss.psfmag_r[m2]
	stars['sdssrerr'] = sdss.psfmagerr_r[m2]
	stars['sdssg'] = sdss.psfmag_g[m2]
	stars['sdssgerr'] = sdss.psfmagerr_g[m2]
	stars['sdssi'] = sdss.psfmag_i[m2]
	stars['sdssierr'] = sdss.psfmagerr_i[m2]

	#quality cuts for g and r
	chilt=1.0
	chigt=0.0
	sharpmed=0.1
	grcolorlt=1.0
	sdssmaggt=18
	sdssmaglt=20	
	sigmacut=3

	#cuts for i
	ricolorlt=1.5


	#select only entries that qualify. use g-r for g and r, r-i only for i.
	#use numpy all, as it tests the truth of every statement within along the given axis.
	if filt == 'g':
                good = np.where(np.all((stars.chi < chilt, stars.chi > chigt, np.absolute(stars.sharp) < sharpmed, stars.sdssg-stars.sdssr < grcolorlt, stars.sdssg < sdssmaglt, stars.sdssg > sdssmaggt),axis=0))
		prepdata = stars[good]
		prepdata = prepdata[idl.binned_sigma(prepdata.sdssg-prepdata.mag, prepdata.sdssg-prepdata.sdssr, bins=5, sigma=2.0)]
	if filt == 'i':
                good = np.where(np.all((stars.chi < chilt, stars.chi > chigt, np.absolute(stars.sharp) < sharpmed, stars.sdssr-stars.sdssi < ricolorlt, stars.sdssi < sdssmaglt, stars.sdssi > sdssmaggt),axis=0))
		prepdata = stars[good]
		prepdata = prepdata[idl.binned_sigma(prepdata.sdssi-prepdata.mag, prepdata.sdssr-prepdata.sdssi, bins=5, sigma=2.0)]

	if filt == 'r':
                good = np.where(np.all((stars.chi < chilt, stars.chi > chigt, np.absolute(stars.sharp) < sharpmed, stars.sdssg-stars.sdssr < grcolorlt, stars.sdssr < sdssmaglt, stars.sdssr > sdssmaggt),axis=0))
		prepdata = stars[good]
		prepdata = prepdata[idl.binned_sigma(prepdata.sdssr-prepdata.mag, prepdata.sdssg-prepdata.sdssr, bins=5, sigma=2.0)]
	

#################################

	print 'Number of selected calibration stars: ' + str(len(prepdata))

	if stars.filter[0] == 'g':
		#make plots
        	fig = plt.figure()
		fig.suptitle(stars.filename[0]+' '+stars.filter[0]+' With Sigma Clipping')
		ax1 = plt.subplot(221)
		ax1.set_xlim([sdssmaggt-1,sdssmaglt+1])
		ax1.set_ylim([np.mean(prepdata.sdssg-prepdata.mag)-1,np.mean(prepdata.sdssg-prepdata.mag)+1])
		ax1.scatter(stars.sdssg, stars.sdssg-stars.mag, c='k', marker='.',s=1)
		ax1.scatter(prepdata.sdssg, prepdata.sdssg-prepdata.mag, c='b')
		ax1.set_xlabel('SDSS g', fontsize=7)
		ax1.set_ylabel('SDSS g - Inst', fontsize=7)
		ax1.tick_params(axis='both', which='major', labelsize=5)
		ax1.tick_params(axis='both', which='minor', labelsize=4)
		ax2 = plt.subplot(222)
		ax2.set_xlim([np.amin(prepdata.sdssg-prepdata.sdssr),grcolorlt+.5])
		ax2.set_ylim([np.mean(prepdata.sdssg-prepdata.mag)-1,np.mean(prepdata.sdssg-prepdata.mag)+1])
		ax2.scatter(stars.sdssg-stars.sdssr, stars.sdssg-stars.mag, c='k', marker='.', s=1)
		ax2.scatter(prepdata.sdssg-prepdata.sdssr, prepdata.sdssg-prepdata.mag, c='r')
		ax2.set_xlabel('SDSS (g-r)',fontsize=7)
		ax2.set_ylabel('SDSS g - Inst',fontsize=7)
		ax2.tick_params(axis='both', which='major', labelsize=5)
		ax2.tick_params(axis='both', which='minor', labelsize=4)
		ax3 = plt.subplot(223)
		ax3.set_xlim([sdssmaggt-1, sdssmaglt+1])
		ax3.set_ylim([chigt, chilt+3])
		ax3.scatter(stars.sdssg, stars.chi, c='k', marker='.',s=1)
		ax3.scatter(prepdata.sdssg, prepdata.chi, c='b')
		ax3.set_ylabel('Chi',fontsize=7)
		ax3.set_xlabel('SDSS g', fontsize=7)
		ax3.tick_params(axis='both', which='major', labelsize=5)
		ax3.tick_params(axis='both', which='minor', labelsize=4)
		ax4 = plt.subplot(224)
		ax4.set_xlim([sdssmaggt-1, sdssmaglt+1])
		ax4.set_ylim([-5*sharpmed,5*sharpmed])
		ax4.scatter(stars.sdssg, stars.sharp, c='k', marker='.',s=1)
		ax4.scatter(prepdata.sdssg, prepdata.sharp, c='b')
		ax4.set_ylabel('Sharp',fontsize=7)
		ax4.set_xlabel('SDSS g', fontsize=7)
		ax4.tick_params(axis='both', which='major', labelsize=5)
		ax4.tick_params(axis='both', which='minor', labelsize=4)
		outputplots.savefig(fig)
		plt.close()

	if stars.filter[0] == 'i':
        	fig = plt.figure()
		fig.suptitle(stars.filename[0]+' '+stars.filter[0]+' With Sigma Clipping')
		ax1 = plt.subplot(221)
		ax1.set_xlim([sdssmaggt-1,sdssmaglt+1])
		ax1.set_ylim([np.mean(prepdata.sdssi-prepdata.mag)-1,np.mean(prepdata.sdssi-prepdata.mag)+1])
		ax1.scatter(stars.sdssi, stars.sdssi-stars.mag, c='k', marker='.')
		ax1.scatter(prepdata.sdssi, prepdata.sdssi-prepdata.mag, c='b')
		ax1.set_xlabel('SDSS i', fontsize=7)
		ax1.set_ylabel('SDSS i - Inst', fontsize=7)
		ax1.tick_params(axis='both', which='major', labelsize=5)
		ax1.tick_params(axis='both', which='minor', labelsize=4)
		ax2 = plt.subplot(222)
		ax2.set_xlim([np.amin(prepdata.sdssr-prepdata.sdssi),ricolorlt+.5])
		ax2.set_ylim([np.mean(prepdata.sdssi-prepdata.mag)-1,np.mean(prepdata.sdssi-prepdata.mag)+1])
		ax2.scatter(stars.sdssr-stars.sdssi, stars.sdssi-stars.mag, c='k', marker='.')
		ax2.scatter(prepdata.sdssr-prepdata.sdssi, prepdata.sdssi-prepdata.mag, c='r')
		ax2.set_xlabel('SDSS (r-i)',fontsize=7)
		ax2.set_ylabel('SDSS i - Inst',fontsize=7)
		ax2.tick_params(axis='both', which='major', labelsize=5)
		ax2.tick_params(axis='both', which='minor', labelsize=4)
		ax3 = plt.subplot(223)
		ax3.set_xlim([sdssmaggt-1, sdssmaglt+1])
		ax3.set_ylim([chigt, chilt+3])
		ax3.scatter(stars.sdssi, stars.chi, c='k', marker='.')
		ax3.scatter(prepdata.sdssi, prepdata.chi, c='b')
		ax3.set_ylabel('Chi',fontsize=7)
		ax3.set_xlabel('SDSS i', fontsize=7)
		ax3.tick_params(axis='both', which='major', labelsize=5)
		ax3.tick_params(axis='both', which='minor', labelsize=4)
		ax4 = plt.subplot(224)
		ax4.set_xlim([sdssmaggt-1, sdssmaglt+1])
		ax4.set_ylim([-5*sharpmed,5*sharpmed])
		ax4.scatter(stars.sdssi, stars.sharp, c='k', marker='.')
		ax4.scatter(prepdata.sdssi, prepdata.sharp, c='b')
		ax4.set_ylabel('Sharp',fontsize=7)
		ax4.set_xlabel('SDSS i', fontsize=7)
		ax4.tick_params(axis='both', which='major', labelsize=5)
		ax4.tick_params(axis='both', which='minor', labelsize=4)
		outputplots.savefig(fig)
		plt.close()

	if stars.filter[0] == 'r':
        	fig = plt.figure()
		fig.suptitle(stars.filename[0]+' '+stars.filter[0]+' With Sigma Clipping')
		ax1 = plt.subplot(221)
		ax1.set_xlim([sdssmaggt-1,sdssmaglt+1])
		ax1.set_ylim([np.mean(prepdata.sdssr-prepdata.mag)-1,np.mean(prepdata.sdssr-prepdata.mag)+1])
		ax1.scatter(stars.sdssr, stars.sdssr-stars.mag, c='k', marker='.')
		ax1.scatter(prepdata.sdssr, prepdata.sdssr-prepdata.mag, c='b')
		ax1.set_xlabel('SDSS r', fontsize=7)
		ax1.set_ylabel('SDSS r - Inst', fontsize=7)
		ax1.tick_params(axis='both', which='major', labelsize=5)
		ax1.tick_params(axis='both', which='minor', labelsize=4)
		ax2 = plt.subplot(222)
		ax2.set_xlim([np.amin(prepdata.sdssg-prepdata.sdssr),grcolorlt+.5])
		ax2.set_ylim([np.mean(prepdata.sdssr-prepdata.mag)-1,np.mean(prepdata.sdssr-prepdata.mag)+1])
		ax2.scatter(stars.sdssg-stars.sdssr, stars.sdssr-stars.mag, c='k', marker='.')
		ax2.scatter(prepdata.sdssg-prepdata.sdssr, prepdata.sdssr-prepdata.mag, c='r')
		ax2.set_xlabel('SDSS (g-r)',fontsize=7)
		ax2.set_ylabel('SDSS r - Inst',fontsize=7)
		ax2.tick_params(axis='both', which='major', labelsize=5)
		ax2.tick_params(axis='both', which='minor', labelsize=4)
		ax3 = plt.subplot(223)
		ax3.set_xlim([sdssmaggt-1, sdssmaglt+1])
		ax3.set_ylim([chigt, chilt+3])
		ax3.scatter(stars.sdssr, stars.chi, c='k', marker='.')
		ax3.scatter(prepdata.sdssr, prepdata.chi, c='b')
		ax3.set_ylabel('Chi',fontsize=7)
		ax3.set_xlabel('SDSS r', fontsize=7)
		ax3.tick_params(axis='both', which='major', labelsize=5)
		ax3.tick_params(axis='both', which='minor', labelsize=4)
		ax4 = plt.subplot(224)
		ax4.set_xlim([sdssmaggt-1, sdssmaglt+1])
		ax4.set_ylim([-5*sharpmed,5*sharpmed])
		ax4.scatter(stars.sdssr, stars.sharp, c='k', marker='.')
		ax4.scatter(prepdata.sdssr, prepdata.sharp, c='b')
		ax4.set_ylabel('Sharp',fontsize=7)
		ax4.set_xlabel('SDSS r', fontsize=7)
		ax4.tick_params(axis='both', which='major', labelsize=5)
		ax4.tick_params(axis='both', which='minor', labelsize=4)
		outputplots.savefig(fig)
		plt.close()

	#write out the data structure to a pickle file for easy use later
	prepdata.dump('../pickles/'+prepdata.filename[0]+'.pkl')


os.chdir('..')


#close the output plot file
outputplots.close()


endingtime = strftime("%Y-%m-%d %H:%M:%S")

print('Start time was ' + startingtime)
print('Ending time was ' + endingtime)
