#Code sample repository for Chris Garling

calibprep.py -- Python code to select well-measured sources from the daophot/allstar photometry output files of our DECam imaging to use later in the maximum likelihood calibration code. 

idlutils.py -- Ports of useful IDL programs to python I wrote and use in my analysis, and a custom function, binned_sigma, which I wrote to do sigma clipping for the calibprep code.

maxlikelihood.pdf -- Document explaining the theory behind the code used to calibrate the Hercules DECam data set.