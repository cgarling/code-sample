from __future__ import division

def file_lines(filename):
    """
    Reads how many lines in a file
    """
    if isinstance(filename,str): filename = open(filename)                  
    lines = 0
    buf_size = 1024 * 1024
    read_filename = filename.read # loop optimization

    buf = read_filename(buf_size)
    while buf:
        lines += buf.count('\n')
        buf = read_filename(buf_size)

    return lines

def read_line(datafile, line):
	line = int(line)
	if isinstance(datafile,file):
        	filedata=datafile.readlines()
		linedata=filedata[line -1]
                linedata=linedata.split()
	elif isinstance(datafile,str):
		datafile = open(datafile, 'r')
        	filedata=datafile.readlines()
		linedata=filedata[line -1]
                linedata=linedata.split()
	else:
		datafile=str(datafile)
		datafile = open(datafile, 'r')
        	filedata=datafile.readlines()
		linedata=filedata[line -1]
                linedata=linedata.split()
		
        return linedata



def glactc(ra, dec, frame='FK5', unit='deg'):
        """
	Converts ra and dec into galactic coordinates like the IDL routine. 

	Parameters:
	-----------
	ra: array-like
		Right Ascension
	dec: array-like
		Declination
	frame: string
		frame in astropy format of the ra and dec coordinate input.
	unit: string   
		unit in astropy format of the ra and dec coordinate input.

	Returns:
	-------
	ell: array-like
		Galactic longitude.
	bee: array like	
		Galactic latitude.
	"""

	import numpy as np
	from astropy.coordinates import SkyCoord, FK5, Galactic
	import astropy.units as u

    	#make sure ra and dec are in correct format for conversion
	if isinstance(ra,np.ndarray):
		raconvert = ra.tolist()
	else: 
		raconvert = ra
	if isinstance(dec, np.ndarray):
		decconvert = dec.tolist()
	
	else:
		decconvert = dec

	#positional frame definition is deprecated.
	#astropycall = 'SkyCoord(raconvert, decconvert, '+frame+', unit=(u.'+unit+', u.'+unit+')).galactic'
	astropycall = 'SkyCoord(raconvert, decconvert, frame='+frame+', unit=(u.'+unit+', u.'+unit+')).galactic'
	galaccoords = eval(astropycall)
	
	ellcommand = 'galaccoords.l.to_string(unit=(u.'+unit+'), decimal=True, precision=7)'
	ell = eval(ellcommand)
	ell = ell.astype(float)
	
	beecommand = 'galaccoords.b.to_string(unit=(u.'+unit+'), decimal=True, precision=7)'
	bee = eval(beecommand)
	bee = bee.astype(float)
	return ell, bee



def binned_sigma(meandata, bindata, sigma=3.0, start=None, end=None, bins=1, mean=False, stderr=False, verbose=False):
	"""
        Takes two correlated data lists, meandata and bindata such that (meandata[1], bindata[1]) descibes a data point. It will trim the lists of data by sigma clipping the meandata list and returning elements that pass the sigma clipping. (On a 2-d plot, can be used to clip y values in bins of x, so that if your function is linearly increasing, the mean of the bin will be more robust for that section of the function than the mean of all the data).
	"""
	import numpy as np
	from scipy.stats import sem
	if start is None: start = np.amin(bindata)
	if end is None: end = np.amax(bindata)
	thisrange = end-start
	step = thisrange / bins
	good = list()
	for iteration in range(bins):
		currentindices = np.where(np.logical_and(bindata >= start,  bindata <= start+step))
		currentmean =  np.mean(meandata[currentindices])
		currentstdev = np.std(meandata[currentindices])
		datarange = currentstdev * sigma
		goodcurrent = np.where(np.logical_and(meandata[currentindices] <= currentmean + datarange, meandata[currentindices] >= currentmean - datarange))
		for i in goodcurrent[0]:
			good.append(currentindices[0][i])
		start = start+step
		if verbose is True: print str( (len(good) / len(meandata)) *100 )+'% of original data retained'
	if mean is True and stderr is True:
		mean = np.mean(meandata[good])
		stderr = sem(meandata[good])
		return good, mean, stderr
	elif mean is True: 
		mean = np.mean(meandata[good])
		return good, mean
	elif stderr is True:
		stderr = sem(meandata[good])
		return good, stderr
	else: 
		return good
		
